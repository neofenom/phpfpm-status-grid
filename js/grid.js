var processGrid = {
    processList: [],
    stats: {},
    updateData: function () {
        var self = this;
        $.ajax({
            url: 'http://uts24.ru/status?json&full',
            type: "get",
            dataType: "json",
            success: function (data) {
                self.stats = {};
                self.processList = [];
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        if (property == 'processes') {
                            self.processList = data[property];
                        } else {
                            self.stats[property] = data[property];
                        }
                    }
                }
                $('.process-list').empty();
                self.drawGrid($('.stats'), $('.process-list'));
                $('table').tablesorter({sortList: [[11,1], [12,1]]} );

            },
            error: function (data) {
               alert('cannot load data');

            }
        });
    },
    drawGrid: function ($statsBlock, $processListBlock) {

        var statsHtml = '';
        statsHtml = '';
        var processListHtml = '';
        processListHtml = '';
        for (var statProp in this.stats) {
            if (this.stats.hasOwnProperty(statProp)) {
                statsHtml += '<div class="stat-property"><strong>' + statProp + '</strong> : ' + this.stats[statProp] + '</div>'
            }
        }
        $statsBlock.html(statsHtml);
        processListHtml += '<table>';
        if (this.processList.length > 0) {
            processListHtml += '<thead><tr class="info-row">';
            for (var infoProp in this.processList[0]) {
                processListHtml += '<th>' + infoProp + '</th>';
            }
            processListHtml += '</tr></thead>'
        }
        processListHtml += '<tbody>';
        for (var pIndex = 0; pIndex < this.processList.length; pIndex++) {
            processListHtml += '<tr class="info-row">';
            for (var processProp in this.processList[pIndex]) {
                if (this.processList[pIndex].hasOwnProperty(processProp)) {
                    processListHtml += '<td>' + this.processList[pIndex][processProp] + '</td>';
                }
            }
            processListHtml += '</tr>'
        }
        processListHtml += '</tbody>';
        processListHtml += '</table>';
        $processListBlock.empty();
        $processListBlock.html(processListHtml);
        console.log(processListHtml);
    }
};






